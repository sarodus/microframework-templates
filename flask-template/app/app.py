from flask import Flask

from .extensions import db
# from .extensions import mongo
from .exceptions import HandledException


class ExampleApp(Flask):
    def __init__(self, import_name='ExampleApp', *args, **kwargs):
        super(ExampleApp, self).__init__(import_name, *args, **kwargs)

        self.setup_config()
        self.setup_extensions()
        self.setup_views()
        self.setup_error_handlers()
        self.setup_commands()

    def setup_config(self):
        self.config.from_object('config')

    def setup_extensions(self):
        db.init_app(self)
        # mongo.init_app(self)

    def setup_views(self):
        from .views.example_api import example_api
        self.register_blueprint(example_api)

    def setup_error_handlers(self):
        @self.errorhandler(HandledException)
        def handle_exceptions(error):
            return "HandledException({})".format(error)

    def setup_commands(self):
        from .commands import register
        register(self)
