from marshmallow import Schema, fields

# could use flask_marshmallow to use Models directly


class HotelSchema(Schema):
    code = fields.Str()
    description = fields.Str()
    rooms = fields.Nested('RoomSchema', many=True, exclude=('hotel',))


class RoomSchema(Schema):
    code = fields.Str()
    description = fields.Str()
    hotel = fields.Nested(HotelSchema, exclude=('rooms',))
