import click
from flask.cli import AppGroup

cli = AppGroup('commands')


@cli.command('example')
def example():
    """Doc example command"""
    print('This is a command example')


@cli.command('example_hello')
@click.argument('name')
def example_hello(name):
    """Doc example hello command"""
    print('Example command with param: name={}'.format(name))
