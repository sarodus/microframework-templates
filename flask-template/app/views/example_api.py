from flask import Blueprint, request, jsonify
from ..schemas.todo import HotelSchema

example_api = Blueprint('example_api', __name__)


@example_api.route('/')
def test_hotels():
    example_data = dict(
        code='hotel1',
        description='A nice hotel',
        rooms=[
            dict(code='SR', description='Single room'),
            dict(code='DR', description='Double room'),
        ]
    )
    return jsonify(
        results=HotelSchema().dump(example_data)
    )
