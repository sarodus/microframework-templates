Example flask app, use as template

To run on development:
- Install the requirements.txt with `pip install -r requirements`, you should do it in a virtualenv

Put these variables and run flask
`export FLASK_APP=manage.py`
`export FLASK_ENV=development`
`flask run`
