from app import ExampleApp
import config

app = ExampleApp(config)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
