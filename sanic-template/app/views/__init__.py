from .base import base
from .other import other

# Maybe autodiscover imports?


blueprints = (
    base,
    other,
)
