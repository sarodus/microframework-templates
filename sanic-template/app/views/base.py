from sanic import Blueprint
from sanic.response import redirect, json


base = Blueprint(__name__, url_prefix='/')


@base.route('/')
async def index(request):
    return await hello(request)


@base.route('/hello')
async def hello(request):
    return json({'hello': 'world'})
