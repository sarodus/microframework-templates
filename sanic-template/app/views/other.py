from sanic import Blueprint
from sanic.response import redirect, json


other = Blueprint(__name__, url_prefix='/other')


@other.route('/')
async def index(request):
    print(request.app.configuration.DB_URI)
    return json({'other': 'route'})
