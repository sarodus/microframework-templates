from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


class Database:
    def __init__(self, app=None, base=None):
        self.base = base
        self.app = None
        self.engine = None
        self.db_session = None
        if app:
            self.init_app(app)

    def init_app(self, app):
        self.engine = create_engine(app.configuration.DB_URI, convert_unicode=True)
        self.db_session = scoped_session(
            sessionmaker(
                autocommit=False,
                autoflush=False,
                bind=self.engine
            )
        )
        self.base.query = self.db_session.query_property()
