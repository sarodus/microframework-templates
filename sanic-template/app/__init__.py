from sanic import Sanic

from .extensions import db
from .views import blueprints


class ExampleApp(Sanic):
    def __init__(self, configuration, name='ExampleApp'):
        super().__init__(name)
        self.debug = True
        self.configuration = configuration

        self.setup_extensions()
        self.setup_blueprints()

    def setup_extensions(self):
        # setup SQLAlchemy connection and Model.query
        db.init_app(self)

    def setup_blueprints(self):
        for blueprint in blueprints:
            self.blueprint(blueprint)
