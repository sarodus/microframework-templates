from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from .ext.database import Database
db = Database(base=Base)
