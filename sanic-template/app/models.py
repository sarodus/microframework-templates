from sqlalchemy import (
    Column,
    ForeignKey,
    String,
    Integer,
    Float
)
from sqlalchemy.orm import relationship
from .extensions import Base


class Cadena(Base):
    __tablename__ = 'hotel_cadena'

    id = Column(Integer, primary_key=True)
    codigo = Column(String(35))


class Hotel(Base):
    __tablename__ = 'hotel_hotel'

    id = Column(Integer, primary_key=True)
    codigo = Column(String(35))
    cadena_id = Column(Integer, ForeignKey(Cadena.id))
    nombre_es = Column(String(100))
    dominio_es = Column(String(100))
    latitud = Column(Float(15))
    longitud = Column(Float(15))
    precio_desde_diario = Column(Float(2))

    cadena = relationship(Cadena)
    fotos = relationship('HotelFoto')


class HotelFoto(Base):
    __tablename__ = 'hotel_hotelfoto'

    id = Column(Integer, primary_key=True)
    imagen = Column(String(300))
    hotel_id = Column(Integer, ForeignKey(Hotel.id))
    hotel = relationship(Hotel)
